FROM openjdk:11-jre-slim
COPY target/*.war /home/app/business-api.war
ENTRYPOINT ["java", "-jar", "/home/app/business-api.war"]
